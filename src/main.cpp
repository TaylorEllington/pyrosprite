#include <iostream>

#include "vulkan/vulkan.h"
#define GLFW_INCLUDE_VULKAN
#include "GLFW/glfw3.h"

#include "vulkan-renderer/vulkan-renderer.h"

int main(char ** argv, int argc){
    
    VulkanRenderer vkRenderer;
    vkRenderer.InitGLFWWindow();
    vkRenderer.InitVulkan();
    vkRenderer.ShutDownVulkan();
    vkRenderer.DestroyGLFWWindow();

    return 0;
}

