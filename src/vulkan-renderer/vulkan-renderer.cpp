#include "vulkan-renderer.h"

bool VulkanRenderer::InitGLFWWindow() {
    glfwInit(); 
    monitors = glfwGetMonitors(&monitorCount);
    const GLFWvidmode * mode = glfwGetVideoMode(monitors[0]);

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // prevent openGL init
    window = glfwCreateWindow(mode->width / 2, mode->height / 2, "test window", nullptr, nullptr);

    glfwShowWindow(window);

    bool test = glfwVulkanSupported();

    return true;
}

bool VulkanRenderer::InitVulkan() {
    uint32_t count;
    const char** extensions = glfwGetRequiredInstanceExtensions(&count);

    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "PyroSprite";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo instanceCreate = {};
    instanceCreate.enabledExtensionCount = count;
    instanceCreate.ppEnabledExtensionNames = extensions;
    instanceCreate.pApplicationInfo = &appInfo;
    instanceCreate.enabledLayerCount = 0; // will need to fix for debug layers

    VkResult result =  vkCreateInstance(&instanceCreate, nullptr, &instance);

    VkResult err = glfwCreateWindowSurface(instance, window, NULL, &surface);

    return true;
}
bool VulkanRenderer::ShutDownVulkan() {

    vkDestroyInstance(instance, nullptr);
    return true;
}
bool VulkanRenderer::DestroyGLFWWindow() {

    glfwDestroyWindow(window);
    return true;
}