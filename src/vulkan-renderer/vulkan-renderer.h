#include "vulkan/vulkan.h"
#define GLFW_INCLUDE_VULKAN
#include "GLFW/glfw3.h"

class VulkanRenderer{

public:
    bool InitGLFWWindow();
    bool InitVulkan();
    bool ShutDownVulkan();
    bool DestroyGLFWWindow();


private:
    GLFWwindow * window;
    GLFWmonitor ** monitors;
    int monitorCount = 0;

    VkInstance instance;
    VkSurfaceKHR surface;
};
